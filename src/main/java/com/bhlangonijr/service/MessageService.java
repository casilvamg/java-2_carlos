package com.bhlangonijr.service;

import com.bhlangonijr.domain.Message;
import com.bhlangonijr.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {
    private static final Logger log = LoggerFactory.getLogger(MessageService.class);
    
    ArrayList<Message> messages = new ArrayList<>();

    /**
     * Validate and store this message into the database
     *
     * @param message
     * @return
     * @throws Exception
     */
    public Response send(Message message) {
        Response response = new Response(); 
        
        if (message.getFrom().contains("mars")){
        	response.setText("Message cannot be sourced by martians!");
        	response.setSuccess(false);
        }
        else {
        	response.setText("hello there - has been processed");
        	response.setSuccess(true);
        }
        
        // ..... validate message and store

        if (response.getSuccess()) {
        	messages.add(message);
        } else {
        //response.setText("Message cannot be sourced by martians!");
        }
        return response;
    }

    public List<Message> getAllMessages() {
        //return new ArrayList<>();
        return messages;
    }

    public Message getById(String id) {
    	for(int i = 0; i < messages.size(); i++) {   
            if (messages.get(i).getId().equals(id)) {
                return messages.get(i);
            }
        }
        return null;
    }
}
